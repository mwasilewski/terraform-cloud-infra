provider "google" {
  project = "${var.project}"
  region  = "${var.region}"
  zone    = "${var.zone}"
}

################################################################################

resource "google_storage_bucket" "cloudfunctions-bucket" {
  name = "cloudfunctions-${var.env}"
}

resource "google_storage_bucket_object" "budget-control" {
  name   = "budget-control-${var.env}"
  bucket = "${google_storage_bucket.cloudfunctions-bucket.name}"
  source = "./scripts/budget-control.py"
}

#resource "google_cloudfunctions_function" "function" {
#  name                  = "function-test"
#  description           = "My function"
#  available_memory_mb   = 128
#  source_archive_bucket = "${google_storage_bucket.bucket.name}"
#  source_archive_object = "${google_storage_bucket_object.archive.name}"
#  trigger_http          = true
#  timeout               = 60
#  entry_point           = "helloGET"
#
#  labels = {
#    my-label = "my-label-value"
#  }
#
#  environment_variables = {
#    MY_ENV_VAR = "my-env-var-value"
#  }
#}

