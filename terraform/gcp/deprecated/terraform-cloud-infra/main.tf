provider "google-beta" {
  project = "${var.project}"
  region  = "${var.region}"
  zone    = "${var.zone}"
}

provider "google" {
  project = "${var.project}"
  region  = "${var.region}"
  zone    = "${var.zone}"
}

# project
################################################################################

# Generate a random id for the project - GCP projects must have globally
# unique names
resource "random_id" "random" {
  prefix      = "${var.project_prefix}"
  byte_length = "8"
}

# Create the project if one isn't specified
resource "google_project" "infra" {
  count           = "${var.project != "" ? 0 : 1}"
  name            = "${random_id.random.hex}"
  project_id      = "${random_id.random.hex}"
  billing_account = "${var.billing_account}"
}

# Or use an existing project, if defined
data "google_project" "infra" {
  count      = "${var.project != "" ? 1 : 0}"
  project_id = "${var.project}"
}

# Obtain the project_id from either the newly created project resource or existing data project resource
# One will be populated and the other will be null
locals {
  infra_project_id = "${element(concat(data.google_project.infra.*.project_id, google_project.infra.*.project_id),0)}"
}

resource "google_project_service" "service" {
  count   = "${length(var.project_services)}"
  project = "${local.infra_project_id}"
  service = "${element(var.project_services, count.index)}"

  # Do not disable the service on destroy. On destroy, we are going to
  # destroy the project, but we need the APIs available to destroy the
  # underlying resources.
  disable_on_destroy = false
}

# networking
################################################################################

resource "google_compute_network" "gke-network" {
  name                    = "gke-network"
  project                 = "${local.infra_project_id}"
  auto_create_subnetworks = false

  depends_on = [
    "google_project_service.service",
  ]
}

resource "google_compute_subnetwork" "gke-subnetwork" {
  name          = "gke-subnetwork"
  project       = "${local.infra_project_id}"
  network       = "${google_compute_network.gke-network.self_link}"
  region        = "${var.region}"
  ip_cidr_range = "${var.kubernetes_network_ipv4_cidr}"

  private_ip_google_access = true

  secondary_ip_range {
    range_name    = "gke-pods"
    ip_cidr_range = "${var.kubernetes_pods_ipv4_cidr}"
  }

  secondary_ip_range {
    range_name    = "gke-svcs"
    ip_cidr_range = "${var.kubernetes_services_ipv4_cidr}"
  }
}

resource "google_compute_address" "infra" {
  name    = "infra-lb"
  region  = "${var.region}"
  project = "${local.infra_project_id}"

  depends_on = ["google_project_service.service"]
}

# Create a NAT router so the nodes can reach DockerHub, etc
resource "google_compute_router" "infra-router" {
  name    = "infra-router"
  project = "${local.infra_project_id}"
  region  = "${var.region}"
  network = "${google_compute_network.gke-network.self_link}"

  bgp {
    asn = 64514
  }
}

resource "google_compute_router_nat" "infra-nat" {
  name    = "infra-nat-1"
  project = "${local.infra_project_id}"
  router  = "${google_compute_router.infra-router.name}"
  region  = "${var.region}"

  nat_ip_allocate_option = "AUTO_ONLY"

  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = "${google_compute_subnetwork.gke-subnetwork.self_link}"
    source_ip_ranges_to_nat = ["PRIMARY_IP_RANGE", "LIST_OF_SECONDARY_IP_RANGES"]

    secondary_ip_range_names = [
      "${google_compute_subnetwork.gke-subnetwork.secondary_ip_range.0.range_name}",
      "${google_compute_subnetwork.gke-subnetwork.secondary_ip_range.1.range_name}",
    ]
  }
}

# gke cluster
################################################################################

data "google_container_engine_versions" "versions" {
  project = "${local.infra_project_id}"
  region  = "${var.region}"
}

# Create the GKE cluster
resource "google_container_cluster" "infra" {
  provider = "google-beta"

  name    = "infra"
  project = "${local.infra_project_id}"
  zone    = "${var.zone}"

  #region  = "${var.region}"

  network            = "${google_compute_network.gke-network.self_link}"
  subnetwork         = "${google_compute_subnetwork.gke-subnetwork.self_link}"
  initial_node_count = "${var.kubernetes_nodes_per_zone}"
  min_master_version = "${data.google_container_engine_versions.versions.latest_master_version}"
  logging_service    = "${var.kubernetes_logging_service}"
  monitoring_service = "${var.kubernetes_monitoring_service}"
  # Disable legacy ACLs. The default is false, but explicitly marking it false
  # here as well.
  enable_legacy_abac = false
  remove_default_node_pool = true
  cluster_autoscaling {
    enabled = true

    resource_limits {
      # number of vCPUs
      resource_type = "cpu"
      minimum       = 2
      maximum       = 20
    }

    resource_limits {
      # amount of RAM in GB
      resource_type = "memory"
      minimum       = 5
      maximum       = 100
    }
  }
  # Configure various addons
  addons_config {
    # Disable the Kubernetes dashboard, which is often an attack vector. The
    # cluster can still be managed via the GKE UI.
    # kubernetes_dashboard {
    #   disabled = true
    # }

    # Enable network policy configurations (like Calico).
    network_policy_config {
      disabled = false
    }
  }
  # Disable basic authentication and cert-based authentication.
  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
  # Enable network policy configurations (like Calico) - for some reason this
  # has to be in here twice.
  network_policy {
    enabled = true
  }
  # Set the maintenance window.
  maintenance_policy {
    daily_maintenance_window {
      start_time = "${var.kubernetes_daily_maintenance_window}"
    }
  }
  # Allocate IPs in our subnetwork
  ip_allocation_policy {
    cluster_secondary_range_name  = "${google_compute_subnetwork.gke-subnetwork.secondary_ip_range.0.range_name}"
    services_secondary_range_name = "${google_compute_subnetwork.gke-subnetwork.secondary_ip_range.1.range_name}"
  }
  # Specify the list of CIDRs which can access the master's API
  master_authorized_networks_config {
    cidr_blocks = ["${var.kubernetes_master_authorized_networks}"]
  }
  # Configure the cluster to be private (not have public facing IPs)
  private_cluster_config {
    # This field is misleading. This prevents access to the master API from
    # any external IP. While that might represent the most secure
    # configuration, it is not ideal for most setups. As such, we disable the
    # private endpoint (allow the public endpoint) and restrict which CIDRs
    # can talk to that endpoint.
    enable_private_endpoint = false

    enable_private_nodes   = true
    master_ipv4_cidr_block = "${var.kubernetes_masters_ipv4_cidr}"
  }
  depends_on = [
    "google_project_service.service",
  ]
}

resource "google_container_node_pool" "primary_pool" {
  project  = "${local.infra_project_id}"
  provider = "google-beta"
  name     = "primary-pool"
  zone     = "${var.zone}"
  cluster  = "${google_container_cluster.infra.name}"
  version  = "${data.google_container_engine_versions.versions.latest_master_version}" # node version cannot be higher than master version

  autoscaling = {
    min_node_count = 2
    max_node_count = 20
  }

  node_config {
    machine_type = "${var.kubernetes_instance_type}"
    preemptible  = "${var.kubernetes_instance_preemptible}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]

    # Set metadata on the VM to supply more entropy
    metadata {
      google-compute-enable-virtio-rng = "true"
    }

    labels {
      service = "primary-pool"
    }

    tags = ["primary-pool"]

    # Protect node metadata
    workload_metadata_config {
      node_metadata = "SECURE"
    }
  }

  management {
    auto_repair  = true
    auto_upgrade = false
  }
}

#resource "google_container_cluster" "primary" {
#  provider = "google-beta"
#  name     = "primary"
#  zone     = "${var.gcp_zone}"
#
#  initial_node_count = 1
#
#  # remove_default_node_pool = true
#  # even though the below is faster, it will keep overwriting the node pool assigned to the cluster
#  #node_pool = [{
#  #  name       = "default-pool"
#  #  node_count = 0
#  #}]
#
#
#  # node_config {
#  #   preemptible = "${var.gcp_preemptible}"
#  # }
#
#  cluster_autoscaling {
#    enabled = true
#
#    resource_limits {
#      # number of vCPUs
#      resource_type = "cpu"
#      minimum       = 2
#      maximum       = 20
#    }
#
#    resource_limits {
#      # amount of RAM in GB
#      resource_type = "memory"
#      minimum       = 5
#      maximum       = 100
#    }
#  }
#  maintenance_policy {
#    daily_maintenance_window {
#      start_time = "03:00"
#    }
#  }
#}
#

# output
################################################################################

output "address" {
  value = "${google_compute_address.infra.address}"
}

output "project" {
  value = "${local.infra_project_id}"
}
