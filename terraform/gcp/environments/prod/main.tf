# provider "google-beta" {
#   version = "~> 2.18.0"
# }

provider "google" {
  version = "~> 2.18.0"
}
# project
################################################################################

# Generate a random id for the project - GCP projects must have globally
# unique names
resource "random_id" "random" {
  prefix      = "${var.project_prefix}"
  byte_length = "8"
}

# Create the project if one isn't specified
resource "google_project" "infra" {
  count           = "${var.project != "" ? 0 : 1}"
  name            = "${random_id.random.hex}"
  project_id      = "${random_id.random.hex}"
  billing_account = "${var.billing_account}"
}

# Or use an existing project, if defined
data "google_project" "infra" {
  count      = "${var.project != "" ? 1 : 0}"
  project_id = "${var.project}"
}

# Obtain the project_id from either the newly created project resource or existing data project resource
# One will be populated and the other will be null
locals {
  infra_project_id = "${element(concat(data.google_project.infra.*.project_id, google_project.infra.*.project_id), 0)}"
}

resource "google_project_service" "service" {
  count   = "${length(var.project_services)}"
  project = "${local.infra_project_id}"
  service = "${element(var.project_services, count.index)}"

  # Do not disable the service on destroy. On destroy, we are going to
  # destroy the project, but we need the APIs available to destroy the
  # underlying resources.
  disable_on_destroy = false
}

# budget control
################################################################################

#module "budget-control" {
#  source      = "../../modules/budget-control/"
#  gcp_sa_file = "${var.gcp_sa_file}"
#  gcp_project = "${var.gcp_project}"
#  gcp_region  = "${var.gcp_region}"
#  gcp_zone    = "${var.gcp_zone}"
#  env         = "${var.env}"
#}

# networking
################################################################################

resource "google_compute_network" "gke-network" {
  name                    = "gke-network"
  project                 = "${local.infra_project_id}"
  auto_create_subnetworks = false

  depends_on = [
    "google_project_service.service",
  ]
}

resource "google_compute_subnetwork" "gke-subnetwork" {
  name          = "gke-subnetwork"
  project       = "${local.infra_project_id}"
  network       = "${google_compute_network.gke-network.self_link}"
  region        = "${var.region}"
  ip_cidr_range = "${var.kubernetes_network_ipv4_cidr}"

  private_ip_google_access = true

  secondary_ip_range {
    range_name    = "gke-pods"
    ip_cidr_range = "${var.kubernetes_pods_ipv4_cidr}"
  }

  secondary_ip_range {
    range_name    = "gke-svcs"
    ip_cidr_range = "${var.kubernetes_services_ipv4_cidr}"
  }
}

# Create a NAT router so the nodes can reach DockerHub, etc
resource "google_compute_router" "infra-router" {
  name    = "infra-router"
  project = "${local.infra_project_id}"
  region  = "${var.region}"
  network = "${google_compute_network.gke-network.self_link}"

  bgp {
    asn = 64514
  }
}

resource "google_compute_router_nat" "infra-nat" {
  name    = "infra-nat-1"
  project = "${local.infra_project_id}"
  router  = "${google_compute_router.infra-router.name}"
  region  = "${var.region}"

  nat_ip_allocate_option = "AUTO_ONLY"

  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = "${google_compute_subnetwork.gke-subnetwork.self_link}"
    source_ip_ranges_to_nat = ["PRIMARY_IP_RANGE", "LIST_OF_SECONDARY_IP_RANGES"]

    secondary_ip_range_names = [
      "${google_compute_subnetwork.gke-subnetwork.secondary_ip_range.0.range_name}",
      "${google_compute_subnetwork.gke-subnetwork.secondary_ip_range.1.range_name}",
    ]
  }
}


# GKE
################################################################################

# https://github.com/terraform-google-modules/terraform-google-gke-gitlab
# https://cloud.google.com/solutions/deploying-production-ready-gitlab-on-gke
# https://github.com/sethvargo/vault-on-gke

data "google_compute_subnetwork" "subnetwork" {
  name    = "gke-subnetwork"
  project = "${local.infra_project_id}"
  region  = "${var.region}"
}

module "gke" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/beta-private-cluster"
  version = "5.1.1"

  project_id              = "${local.infra_project_id}"
  name                    = "${var.cluster_type}-gke-cluster"
  regional                = false
  region                  = "${var.region}"
  zones                   = "${var.zones}"
  network                 = "gke-network"
  subnetwork              = "gke-subnetwork"
  ip_range_pods           = "gke-pods"
  ip_range_services       = "gke-svcs"
  enable_private_endpoint = false
  enable_private_nodes    = true
  master_ipv4_cidr_block  = "172.16.0.0/28"
  istio                   = false # will self-install istio instead to be cloud agnostic and to save cash
  cloudrun                = false # will use knative instead to be cloud agnostic and to save cash
  logging_service         = "none"
  monitoring_service      = "none"

  master_authorized_networks_config = [
    {
      cidr_blocks = [
        {
          cidr_block   = "31.186.202.98/32"
          display_name = "waw"
        },
      ]
    },
  ]

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "n1-standard-2"
      min_count          = 1
      max_count          = 1
      disk_size_gb       = 10
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = true
      initial_node_count = 1
    },
  ]

}
