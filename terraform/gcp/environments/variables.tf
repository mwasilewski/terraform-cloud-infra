variable "project" {
  type    = "string"
  default = ""

  description = <<EOF
Project ID where Terraform is authenticated to run to create additional
projects.
EOF
}

variable "project_prefix" {
  type = "string"
  default = "infra-"

  description = <<EOF
String value to prefix the generated project ID with.
EOF
}

variable "region" {
  type    = "string"
  default = "us-central1"

  description = <<EOF
Region in which to create the cluster
EOF
}

variable "zones" {
  type = "list"
  default = ["us-central1-c"]

  description = <<EOF
Zone in which to create the cluster
EOF
}

variable "project_services" {
  type = "list"

  default = [
    #"cloudkms.googleapis.com",
    "cloudresourcemanager.googleapis.com",

    "container.googleapis.com",
    "compute.googleapis.com",
    "iam.googleapis.com",
    "logging.googleapis.com",
    "monitoring.googleapis.com",
  ]
}

variable "billing_account" {
  type = "string"

  description = <<EOF
Billing account ID.
EOF
}


variable "env" {
  type = "string"

  description = <<EOF
env variable which allows to differentiate the same resources in different environments
EOF
}

# GKE
################################################################################

variable "kubernetes_network_ipv4_cidr" {
  type    = "string"
  default = "10.0.4.0/22"

  description = <<EOF
IP CIDR block for the subnetwork. This must be at least /22 and cannot overlap
with any other IP CIDR ranges.
EOF
}

variable "kubernetes_pods_ipv4_cidr" {
  type = "string"
  default = "10.1.0.0/16"

  description = <<EOF
IP CIDR block for pods. This must be at least /22 and cannot overlap with any
other IP CIDR ranges.
EOF
}

variable "kubernetes_services_ipv4_cidr" {
  type    = "string"
  default = "10.2.0.0/16"

  description = <<EOF
IP CIDR block for services. This must be at least /22 and cannot overlap with
any other IP CIDR ranges.
EOF
}

variable "istio" {
  description = "Boolean to enable / disable Istio"
  default = true
}

variable "cloudrun" {
  description = "Boolean to enable / disable CloudRun"
  default = true
}

variable "cluster_type" {

}
