#!/bin/sh

#  bash heredoc:
#  $ cat > test.sh
#
#######################################

name=storj-down
url='http://alertmanager-operated.observability.svc.cluster.local:9093/api/v1/alerts'
now="$(date --rfc-3339=seconds | sed 's/ /T/')"





# echo "firing up alert $name"
# curl -XPOST $url -d "[{
# 	\"labels\": {
# 		\"alertname\": \"$name\",
# 		\"severity\":\"critical\"
# 	},
# 	\"annotations\": {
# 		\"summary\": \"High latency is high!\"
# 	},
#   \"startsAt\": \"$now\"
# }]"
# echo ""



#echo "press enter to resolve alert"
#read


echo "sending resolve"
curl -XPOST $url -d "[{
	\"labels\": {
		\"alertname\": \"$name\",
		\"severity\":\"critical\"
	},
	\"annotations\": {
		\"summary\": \"High latency is high!\"
	},
  \"startsAt\": \"$now\",
  \"endsAt\": \"$now\"
}]"
echo ""
