# terraform-cloud-infra

## Authenticating Terraform to GCP ##

service accounts cannot create projects without a parent (organization or folder). In order to use an organization or folder you need have a GSuite domain in GCP or activate Cloud Identity (which is paid per user/month)

a workaround is to use user credentials rather than a service account

`terraform init`

`gcloud auth application-default login` (make sure GOOGLE_APPLICATION_CREDENTIALS is not set)

`terraform apply`

## terraform module for GKE ##

not using this module: `https://registry.terraform.io/modules/terraform-google-modules/kubernetes-engine/google/0.4.0` because it doesn't support autoscalling (which is a beta feature)

## access from the Internet ##

the entire cluster is on private networks, the only connectivity it has with the outside world is through a NAT and a load balancing service in k8s.

After creating the cluster with `terraform apply`, you will see output with the address of the LB service. Copy it and put it in the helm values so that the LB service that will be created uses it


## GCP readme ##

It's easier to keep all of your resources in a separate GCP project, because you can easily remove all objects by deleting the project. Otherwise you might be billed for some left over public ip addresses or disks which were automatically created with the default policy of keeping them after the VM/cluster deletion.

The preferred way of authentication with Terraform to GCP is using service accounts + env var so that the terraform binary know where the json file with the key is

you also need to enable a number of API's (terraform will error if they are not enabled with a link to enabling them)
